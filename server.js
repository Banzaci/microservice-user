var io 					= require('socket.io-client'),
	db    				= require('./db/user'),
	User    			= require('./schema/user'),
    socket 				= io.connect('http://localhost:3000'),
    USER_BY_ID 			= "userById",
    ADD_NEW_USER 		= "addNewUser",
    ALL_USERS 			= "allUsers",
    CONNECT 			= "connect",
    DISCONNECT 			= "disconnect",
    funcs				= [USER_BY_ID, ADD_NEW_USER, ALL_USERS]; 

socket.on(ALL_USERS, function( data, fn ){
	User.find({}).sort('firstName').exec(function(err, response) {
		fn(response);
	});
});

socket.on(ADD_NEW_USER, function( data, fn ){
	var response;
	new User(data.user).save(function(err, thread) {
		response = (err === null) ? thread : err;
		fn(response); 
	});
}); 

socket.on(CONNECT, function(){
	console.log(CONNECT + ":" + funcs);
	socket.emit('func', { funcs:funcs }); 
});

socket.on(DISCONNECT, function(){
	console.log(DISCONNECT + ":" + funcs);
});